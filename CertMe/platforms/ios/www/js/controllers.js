angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, $state, $ionicLoading, $ionicPopup) {
  $scope.login = function(loginData){
	$ionicLoading.show();
	setTimeout(function(){
		$state.go('app.profile');
	}, 1000);
//	if(loginData.username == "raulvbrito" && loginData.password == "1234567"){
//		$state.go('app.profile');
//		$ionicLoading.hide();
//	}else{
//		$ionicLoading.hide();
//		$ionicPopup.alert({
//			title: 'Erro de Autenticação',
//			template: 'Usuário ou Senha incorretos'
//		});
//	}
  }
})

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http) {
  $ionicModal.fromTemplateUrl('templates/cert-modal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  $scope.certModal = function(certificateName, universityName, universityLogo, hash_cert){
	$scope.certificateName = certificateName;
	$scope.universityName = universityName;
	$scope.universityLogo = universityLogo;
	
	$http.get('http://192.168.1.33:3000/api/certificados/'+hash_cert).then(function(success){
		console.log(success);
	}, function(error){
		console.log(error);
	});
	
	$scope.modal.show();
  }
  
  $ionicModal.fromTemplateUrl('templates/qrcode.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.qrCodeModal = modal;
  });

  $scope.closeQRCodeModal = function() {
    $scope.qrCodeModal.hide();
  };

  $scope.qrCode = function(){
	$scope.qrCodeModal.show();
  }
})

.controller('CurriculaCtrl', function($scope) {
  
})

.controller('ProfileCtrl', function($scope, $stateParams, $ionicModal, $http, $ionicLoading, $interval) {
	var count = 0;
	var certLength = 0;
//	$interval(function() {
	  $http.get('http://192.168.1.33:3000/api/aluno').then(function(success){
		console.log(success);
		$scope.certificados = success.data.certificados;
		if(success.data){
			if(count > 0){
				if(success.data.certificados.length > certLength){
					$ionicPopup.alert({
						title: 'Atenção',
						template: 'Novo diploma listado'
					});
				}
				certLength = success.data.certificados.length;
				count++;
			}else{
				certLength = success.data.certificados.length;
				count++;
			}
		}
		$ionicLoading.hide();
	  }, function(error){
		console.log(error);
		$ionicLoading.hide();
	  });
//	}, 3000);
	
  $scope.doRefresh = function(){
	  $http.get('http://192.168.1.33:3000/api/aluno').then(function(success){
		console.log(success);
		$scope.certificados = success.data.certificados;
		if(success.data){
			if(count > 0){
				if(success.data.certificados.length > certLength){
					$ionicPopup.alert({
						title: 'Atenção',
						template: 'Novo diploma listado'
					});
				}
				certLength = success.data.certificados.length;
				count++;
			}else{
				certLength = success.data.certificados.length;
				count++;
			}
		}
		$scope.$broadcast('scroll.refreshComplete');
	  }, function(error){
		console.log(error);
		$scope.$broadcast('scroll.refreshComplete');
		$ionicLoading.hide();
	  });
  }
});
